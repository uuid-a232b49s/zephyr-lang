int main(int argc, char ** argv) {
    static volatile const char * fn = CURRENT_FUNCTION;
    return (fn != 0) ? 0 : -1;
}
