cmake_minimum_required(VERSION 3.14)
cmake_policy(VERSION 3.14)



# #
# preset dirs

set(_gen_dir "${CMAKE_CURRENT_BINARY_DIR}/generated")
set(_gen_intern_dir "${_gen_dir}/include-intern/zephyr-lang/internal")
set(_gen_incl_public_dir "${_gen_dir}/include/zephyr-lang")
file(MAKE_DIRECTORY "${_gen_intern_dir}" "${_gen_incl_public_dir}/internal")

# sources
set(_extra_sources "")



# #
# generate decl-ctor.hpp

set(_data_ "")
set(_data_margs "")
set(_data_args "")
set(_data_init "")
foreach(_v RANGE 0 8)
    math(EXPR _vn "${_v}+1")
    string(APPEND _data_margs ", arg${_v}")
    list(APPEND _data_args "decltype(arg${_v}) arg${_v}")
    list(JOIN _data_args ", " _data_args_)
    list(APPEND _data_init "arg${_v}(std::move(arg${_v}))")
    list(JOIN _data_init ", " _data_init_)
    if("${_vn}" STREQUAL "1")
        set(_explicit_ "explicit")
    endif()
    string(APPEND _data_
        "#define ZEPHYRLANG_DECL_CTOR_${_vn}(type${_data_margs}) ${_explicit_} type(${_data_args_}) : ${_data_init_} {}\n"
    )
    unset(_explicit_)
endforeach()
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/include-intern/zephyr-lang/internal/decl-ctor.hpp.in"
    "${_gen_incl_public_dir}/internal/decl-ctor.hpp"
    @ONLY
)



# #
# generate aggregate headers

foreach(_namespace_ IN ITEMS syntactic;semantic)
    _zephyrlang_collect_headers(
        "${CMAKE_CURRENT_SOURCE_DIR}/include/zephyr-lang/${_namespace_}"
        "${CMAKE_CURRENT_SOURCE_DIR}/include"
        _includes_
    )
    string(TOUPPER ${_namespace_} _NAMESPACE_)
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/include-intern/zephyr-lang/aggregate.hpp.in"
        "${_gen_incl_public_dir}/${_namespace_}.hpp"
        @ONLY
    )
endforeach()



# #
# pretty-function
_zephyrlang_find_pretty_func(_pretty_func
    __PRETTY_FUNCTION__
    __FUNCTION__
    __func__
)



# #
# generate listeners

set(_semantic_includes_ "\n#include \"zephyr-lang/strings.hpp\"")
set(_semantic_extra_ "\n            ZEPHYRLANG_API bool GetIdentifier(Listenable &, sstr & out);\n")
unset(_syntactic_includes_)
unset(_syntactic_extra_)

foreach(_namespace_ IN ITEMS syntactic;semantic)
    set(_fwd_decl_types_ "")
    set(_fwd_decl_methods_ "")
    set(_def_methods_ "")
    set(_def_includes_ "")
    string(TOUPPER ${_namespace_} _NAMESPACE_)

    file(GLOB_RECURSE _hh LIST_DIRECTORIES FALSE "${CMAKE_CURRENT_SOURCE_DIR}/include/zephyr-lang/${_namespace_}/*.hpp")

    foreach(_h IN LISTS _hh) # iterate over header files
        file(READ "${_h}" _f)
        string(REGEX MATCHALL "ZEPHYRLANG_LISTEN_TYPE\\([a-zA-Z0-0_:]+\\)" _f "${_f}")
        string(REGEX REPLACE "ZEPHYRLANG_LISTEN_TYPE|[)(]" "" _f "${_f}")

        foreach(_decl IN LISTS _f) # iterate over ZEPHYRLANG_LISTEN_TYPE markers
            _zephyrlang_fwddecl_qualname(${_decl} "            " _fwd_decl_type_)

            set(_fwd_decl_method_ "
                virtual signed long long visit(${_decl} &);
                virtual bool begin(${_decl} &);
                virtual void end(${_decl} &);"
            )

            set(_def_method_ "
                signed long long Listener::visit(${_decl} & x) { return visit_default(x); }
                bool Listener::begin(${_decl} & x) { return begin_default(x); }
                void Listener::end(${_decl} &) {}"
            )

            # append results
            set(_fwd_decl_types_ "${_fwd_decl_types_}\n${_fwd_decl_type_}")
            set(_fwd_decl_methods_ "${_fwd_decl_methods_}\n${_fwd_decl_method_}")
            set(_def_methods_ "${_def_methods_}\n${_def_method_}")
        endforeach()
        set(_def_includes_ "${_def_includes_}\n#include \"${_h}\"")
    endforeach()

    set(_extra_ "${_${_namespace_}_extra_}")
    set(_includes_ "${_${_namespace_}_includes_}")

    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/include-intern/zephyr-lang/listener.hpp.in"
        "${_gen_incl_public_dir}/${_namespace_}/listener.hpp"
        @ONLY
    )
    set(_s "${CMAKE_CURRENT_BINARY_DIR}/${_namespace_}/listener.cxx")
    configure_file(
        "${CMAKE_CURRENT_SOURCE_DIR}/include-intern/zephyr-lang/listener.cxx.in"
        "${_s}"
        @ONLY
    )
    list(APPEND _extra_sources "${_s}")
endforeach()



# #
# generate parser & lexer files

set(_patch "${CMAKE_CURRENT_SOURCE_DIR}/grammar/patch.cmake")

set(_s "${_gen_intern_dir}/parser.cxx")
set(_h "${_gen_intern_dir}/parser.hxx")
set(_in "${CMAKE_CURRENT_SOURCE_DIR}/grammar/parser.y")
add_custom_command(
    OUTPUT "${_s}" "${_h}"
    COMMAND "${BISON_EXECUTABLE}" ARGS
        --feature=none --color=no --warnings=all
        #-Wcounterexamples
        "--header=${_h}"
        "--output=${_s}"
        "${_in}"
    COMMAND "${CMAKE_COMMAND}" ARGS
        "-D_file=${_h}"
        "-D_type=parser-header"
        "-D_bin_dir=${CMAKE_CURRENT_BINARY_DIR}"
        -P "${_patch}"
    COMMAND "${CMAKE_COMMAND}" ARGS
        "-D_file=${_s}"
        "-D_type=parser-source"
        "-D_bin_dir=${CMAKE_CURRENT_BINARY_DIR}"
        -P "${_patch}"
    DEPENDS "${_in}"
)
list(APPEND _extra_sources "${_s}")

set(_s "${_gen_intern_dir}/lexer.cxx")
set(_in "${CMAKE_CURRENT_SOURCE_DIR}/grammar/lexer.l")
add_custom_command(
    OUTPUT "${_s}"
    COMMAND "${FLEX_EXECUTABLE}" ARGS
        --prefix=zephyr_lang_internal_
        "--outfile=${_s}"
        "${_in}"
    DEPENDS "${_in}"
)
list(APPEND _extra_sources "${_s}")
