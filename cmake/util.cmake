cmake_minimum_required(VERSION 3.14)
cmake_policy(VERSION 3.14)

macro(_zephyrlang_collect_headers scan_dir rel_incl_dir out)
    set(${out} "")
    file(GLOB_RECURSE _hh "${scan_dir}/*.hpp")
    foreach(_h IN LISTS _hh)
        file(RELATIVE_PATH _hr "${rel_incl_dir}" "${_h}")
        string(APPEND ${out} "#include \"${_hr}\"\n")
    endforeach()
endmacro()

macro(_zephyrlang_find_pretty_func out)
    unset(${out})
    foreach(fn IN ITEMS ${ARGN})
        set(_r _zephyrlang_find_pretty_func_${fn})
        try_compile(${_r}
            "${CMAKE_CURRENT_BINARY_DIR}/pretty-function"
            SOURCES "${CMAKE_CURRENT_LIST_DIR}/pretty-function.cxx"
            COMPILE_DEFINITIONS -DCURRENT_FUNCTION=${fn}
        )
        if(${${_r}})
            set(${out} ${fn})
            break()
        endif()
    endforeach()
endmacro()

macro(_zephyrlang_fwddecl_qualname qname indent out)
    # qname to list
    string(REPLACE "::" ";" _ns "${qname}")
    list(LENGTH _ns _l)
    math(EXPR _l "${_l} - 1")
    list(GET _ns ${_l} _s) # unqualified name
    list(REMOVE_AT _ns ${_l})

    # iterate over namespaces
    set(${out} "${indent}")
    foreach(_nse IN LISTS _ns)
        set(${out} "${${out}}namespace ${_nse} { ")
    endforeach()
    set(${out} "${${out}}struct ${_s};")
    foreach(_nse IN LISTS _ns)
        set(${out} "${${out}} }")
    endforeach()
endmacro()
