zephyr-lang
***********


What is this?
=============

- a programming language

- inspired by *C++*, *C#* and *Java*

- statically-typed, garbage-collected

- compiled, LLVM based


How to use this?
================

- no, its too early


Building
========

Requirements
------------

- environment

  - cmake
      *>3.14*

- building

  - llvm
      *tested with 16.0.3*

  - bison
      *>3.6*

  - flex
      *tested with 2.6.4*

Compilation
-----------

- init cmake cache

  .. code-block:: bash

    $ mkdir _build
    $ cd _build
    $ cmake -DCMAKE_PREFIX_PATH=<path to llvm installation> .. -DCMAKE_INSTALL_PREFIX=<libs installation path>

- build

  .. code-block:: bash

    $ cd _build
    $ cmake --build .

Installation
------------

- if ``CMAKE_INSTALL_PREFIX`` was specified during cache generation, then the project can be installed

  .. code-block:: bash

    $ cd _build
    $ cmake --build . --target install
