#include <utility>

#include "zephyr-lang/strings.hpp"

namespace zephyr {
    namespace lang {

        sstr strings::get(const char * s) {
            {
                auto i = m_strings.find(s);
                if (i != m_strings.end()) {
                    return i->second;
                }
            }
            {
                sstr v { new std::string(s) };
                return m_strings[v->c_str()] = std::move(v);
            }
        }

    }
}
