namespace zephyr {
    namespace lang {
        namespace semantic {
            namespace internal {

                struct store_fn {
                    virtual void operator()(semantic::Function *, syntactic::Function *) = 0;
                };

                struct FunctionPass : syntactic::Listener {
                    Module * m_module = nullptr;
                    errors * m_err = nullptr;
                    std::vector<Global *> m_path;
                    store_fn * m_store_fn;

                    FunctionPass(Module * module, errors * err, store_fn * store_fn)
                        : m_module(module), m_err(err), m_store_fn(store_fn) {}

                    virtual bool begin(syntactic::CompilationUnit &) override {
                        m_path.emplace_back(m_module);
                        return true;
                    }

                    virtual void end(syntactic::CompilationUnit &) override {
                        ASSERT(m_path.size() == 1 && m_path.back() == m_module, assert_, "unexpected CU end");
                        m_path.clear();
                    }

                    virtual bool begin(syntactic::Namespace & x) override {
                        ASSERT(!m_path.empty(), assert_, "unexpected empty path");
                        decltype(Namespace::declarations) * decls = nullptr;
                        semantic::visit<Module, Namespace>( // get <decls> vector reference
                            *m_path.back(),
                            [&] (Module & m) { decls = &m.declarations; },
                            [&] (Namespace & n) { decls = &n.declarations; },
                            [] { THROW_ICE("unexpected node"); }
                        );
                        Namespace * ns = nullptr;
                        findByIdentifier(x.identifier, decls->begin(), decls->end(),
                            [&] (const std::unique_ptr<Global> & a) {
                                semantic::visit<Namespace>(*a,
                                    [&] (Namespace & n) { ASSERT_ICE(!ns, "unexpected duplicate namespace"); ns = &n; }
                                );
                            }
                        );
                        ASSERT_ICE(ns, "namespace not found");
                        m_path.emplace_back(ns);
                        return true;
                    }

                    virtual void end(syntactic::Namespace & x) override {
                        ASSERT(m_path.size() >= 2, assert_, "unexpected namespace end");
                        semantic::visit<Namespace>(*m_path.back(),
                            [&] (Namespace & ns) {
                                ASSERT(*ns.identifier == *x.identifier, assert_, "namespace mismatch");
                            },
                            [] { THROW(assert_, "unexpected node"); }
                        );
                        m_path.pop_back();
                    }

                    virtual bool begin(syntactic::Function & x) override {
                        ASSERT(!m_path.empty(), assert_, "unexpected empty path");
                        auto & decls = getDecls(*m_path.back());
                        Function * fn = nullptr;
                        bool hasErr = false;
                        findByIdentifier(x.identifier, decls.begin(), decls.end(),
                            [&] (const std::unique_ptr<Global> & a) {
                                semantic::visit<Function>(*a,
                                    [&] (Function & f) { if (fn) m_err->onError("function redefinition", x, f); fn = &f; },
                                    [&] { m_err->onError("duplicate identifier", x, *a); hasErr = true; }
                                );
                            }
                        );
                        if (!hasErr && !fn) {
                            decls.emplace_back(fn = new Function());
                            fn->identifier = x.identifier;
                            fn->container = m_path.back();
                            (*m_store_fn)(fn, &x);
                        }
                        return false;
                    }

                };

            }
        }
    }
}
