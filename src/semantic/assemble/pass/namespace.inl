namespace zephyr {
    namespace lang {
        namespace semantic {
            namespace internal {

                struct NamespacePass : syntactic::Listener {
                    Module * m_module = nullptr;
                    errors * m_err = nullptr;
                    std::vector<Global *> m_path;

                    NamespacePass(Module * module, errors * err) : m_module(module), m_err(err) {}

                    virtual bool begin(syntactic::CompilationUnit &) override {
                        m_path.emplace_back(m_module);
                        return true;
                    }

                    virtual void end(syntactic::CompilationUnit &) override {
                        ASSERT(m_path.size() == 1 && m_path.back() == m_module, assert_, "unexpected CU end");
                        m_path.clear();
                    }

                    virtual bool begin(syntactic::Namespace & x) override {
                        ASSERT(!m_path.empty(), assert_, "unexpected empty path");
                        auto & decls = getDecls(*m_path.back());
                        Namespace * ns = nullptr;
                        findByIdentifier(x.identifier, decls.begin(), decls.end(),
                            [&] (const std::unique_ptr<Global> & a) {
                                semantic::visit<Namespace>(*a,
                                    [&] (Namespace & n) { ASSERT_ICE(ns != &n, "unexpected duplicate namespace"); ns = &n; },
                                    [] { THROW_ICE("unexpected node"); }
                                );
                            }
                        );
                        if (!ns) {
                            std::unique_ptr<Namespace> newNs { ns = new Namespace() };
                            newNs->container = m_path.back();
                            newNs->identifier = x.identifier;
                            decls.emplace_back(std::move(newNs));
                        }
                        m_path.emplace_back(ns);
                        return true;
                    }

                    virtual void end(syntactic::Namespace & x) override {
                        ASSERT(m_path.size() >= 2, assert_, "unexpected namespace end");
                        semantic::visit<Namespace>(*m_path.back(),
                            [&] (Namespace & ns) {
                                ASSERT(*ns.identifier == *x.identifier, assert_, "namespace mismatch");
                            },
                            [] { THROW(assert_, "unexpected node"); }
                        );
                        m_path.pop_back();
                    }

                };

            }
        }
    }
}
