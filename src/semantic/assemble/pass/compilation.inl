namespace zephyr {
    namespace lang {
        namespace semantic {
            namespace internal {

                struct FnCompilationPass : syntactic::Listener {
                    semantic::Function * m_semFn;
                    syntactic::Function * m_synFn;
                    errors * m_err;

                    FnCompilationPass(semantic::Function * semFn, syntactic::Function * synFn, errors * err)
                        : m_semFn(semFn), m_synFn(synFn), m_err(err) {}

                    virtual signed long long visit_default(syntactic::Listenable &) override { THROW_ICE("unexpected compilation node"); }
                    virtual bool begin_default(syntactic::Listenable &) override { THROW_ICE("unexpected compilation node"); }

                    virtual bool begin(syntactic::Function &) override {
                        return true;
                    }
                    virtual void end(syntactic::Function &) override {}

                    virtual bool begin(syntactic::expr::Assign &) override { return true; }
                    virtual void end(syntactic::expr::Assign &) override {}

                    virtual bool begin(syntactic::expr::Binary & x) override { return true; }
                    virtual void end(syntactic::expr::Binary &) override {}

                    virtual bool begin(syntactic::expr::Compare &) override { return true; }
                    virtual void end(syntactic::expr::Compare &) override {}

                    virtual bool begin(syntactic::expr::Identifier &) override { return true; }
                    virtual void end(syntactic::expr::Identifier &) override {}

                    virtual bool begin(syntactic::expr::Invoke &) override { return true; }
                    virtual void end(syntactic::expr::Invoke &) override {}

                    virtual bool begin(syntactic::expr::Lambda &) override { return true; }
                    virtual void end(syntactic::expr::Lambda &) override {}

                    virtual bool begin(syntactic::expr::Literal &) override { return true; }
                    virtual void end(syntactic::expr::Literal &) override {}

                    virtual bool begin(syntactic::expr::StringLiteral &) override { return true; }
                    virtual void end(syntactic::expr::StringLiteral &) override {}

                    virtual bool begin(syntactic::expr::MemberAccess &) override { return true; }
                    virtual void end(syntactic::expr::MemberAccess &) override {}

                    virtual bool begin(syntactic::expr::Unary &) override { return true; }
                    virtual void end(syntactic::expr::Unary &) override {}

                };

            }
        }
    }
}
