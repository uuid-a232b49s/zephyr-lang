#ifndef HEADER_ZEPHYRLANG_SRC_SEMANTIC_COMMON
#define HEADER_ZEPHYRLANG_SRC_SEMANTIC_COMMON 1

#include "zephyr-lang/strings.hpp"
#include "zephyr-lang/semantic/module.hpp"

namespace zephyr {
    namespace lang {
        namespace semantic {
            namespace internal {

                template<typename itr_t, typename fn_on_find_t>
                void findByIdentifier(const sstr & identifier, itr_t begin, itr_t end, fn_on_find_t fn_on_find) {
                    for (auto itr = begin; itr != end; ++itr) { // check if the current <decls> already contains the namespace
                        sstr id;
                        if (GetIdentifier(**itr, id) && *id == *identifier) {
                            fn_on_find(*itr);
                        }
                    }
                }


                decltype(Namespace::declarations) & getDecls(Global & x) {
                    decltype(Namespace::declarations) * decls;
                    semantic::visit<Module, Namespace>(
                        x,
                        [&] (Module & m) { decls = &m.declarations; },
                        [&] (Namespace & n) { decls = &n.declarations; },
                        [] { THROW_ICE("unexpected node"); }
                    );
                    return *decls;
                }

            }
        }
    }
}

#endif
