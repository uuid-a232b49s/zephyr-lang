#include <vector>

#include "zephyr-lang/strings.hpp"

#include "zephyr-lang/syntactic.hpp"

#include "zephyr-lang/semantic/module.hpp"
#include "zephyr-lang/semantic/listener.hpp"
#include "zephyr-lang/semantic/function.hpp"
#include "zephyr-lang/semantic/namespace.hpp"

#include "throw_.hxx"

#include "common.hxx"
#include "pass/namespace.inl"
#include "pass/functions.inl"
#include "pass/compilation.inl"

namespace zephyr {
    namespace lang {
        namespace semantic {

            std::unique_ptr<Module> Module::assemble(CuContainer & cui, errors & err) {
                auto cuCount = cui.count();
                std::vector<std::pair<semantic::Function *, syntactic::Function *>> functions;

                std::unique_ptr<Module> m { new Module() };

                {
                    internal::NamespacePass pass { m.get(), &err };
                    for (decltype(cuCount) i = 0; i < cuCount; ++i) {
                        syntactic::listen(pass, cui.at(i));
                    }
                }
                {
                    struct store_fn0 : internal::store_fn {
                        decltype(functions) * m_functions;
                        virtual void operator()(semantic::Function * a, syntactic::Function * b) override {
                            m_functions->emplace_back(a, b);
                        }
                    } store_fn;
                    store_fn.m_functions = &functions;
                    internal::FunctionPass pass { m.get(), &err, &store_fn };
                    for (decltype(cuCount) i = 0; i < cuCount; ++i) {
                        syntactic::listen(pass, cui.at(i));
                    }
                }
                {
                    for (auto & x : functions) {
                        internal::FnCompilationPass pass { x.first, x.second, &err };
                        syntactic::listen(pass, *x.second);
                    }
                }

                return m;
            }

        }
    }
}
