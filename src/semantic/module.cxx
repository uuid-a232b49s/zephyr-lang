#include "zephyr-lang/semantic/module.hpp"

namespace zephyr {
    namespace lang {
        namespace semantic {

            syntactic::CompilationUnit & CuVector::at(unsigned long long i) {
                return *std::vector<std::unique_ptr<syntactic::CompilationUnit>>::at(i);
            }

            unsigned long long CuVector::count() {
                return std::vector<std::unique_ptr<syntactic::CompilationUnit>>::size();
            }

        }
    }
}
