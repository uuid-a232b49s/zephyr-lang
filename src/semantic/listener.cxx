#include "listener.hxx"

#include "zephyr-lang/strings.hpp"
#include "zephyr-lang/semantic/listener.hpp"

namespace zephyr {
    namespace lang {
        namespace semantic {

            void listen(Listener & listener, Listenable & listenable) {
                return internal::listen<details::ListenableList>(listener, listenable);
            }

        }
    }
}

#include "zephyr-lang/semantic/function.hpp"
#include "zephyr-lang/semantic/namespace.hpp"

namespace zephyr {
    namespace lang {
        namespace semantic {

            bool GetIdentifier(Listenable & listenable, sstr & out) {
                return visit<Function, Namespace>(listenable,
                    [&] (Function & x) { out = x.identifier; return 1; },
                    [&] (Namespace & x) { out = x.identifier; return 1; }
                ) == 1;
            }

        }
    }
}
