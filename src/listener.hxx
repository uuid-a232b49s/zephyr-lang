#ifndef HEADER_ZEPHYRLANG_INTERNAL_LISTENER
#define HEADER_ZEPHYRLANG_INTERNAL_LISTENER 1

#include <vector>

namespace zephyr {
    namespace lang {
        namespace internal {

            template<typename ListenableList, typename Listener, typename Listenable>
            void listen(Listener & listener, Listenable & listenable) {
                using namespace std;
                using S = size_t;

                struct element_t {
                    Listenable * target;
                    S nested;
                };
                vector<element_t> stack;

                struct list : ListenableList, vector<Listenable *> {
                    virtual void add(Listenable & l) override { vector<Listenable *>::emplace_back(&l); }
                } list;

                stack.emplace_back(element_t { &listenable, 0 });
                for (S i = 0;;) {
                    auto & e = stack.at(i);
                    if (e.nested != 1) {
                        if (e.nested == 0) {
                            if (e.target->begin(listener, list)) {
                                if ((e.nested = 1 + list.size()) != 1) {
                                    for (S j = 0, l = list.size(); j < l; ++j) {
                                        stack.insert(stack.begin() + i + j + 1, element_t { list[j], 0 });
                                    }
                                    list.clear();
                                    ++i;
                                    continue;
                                }
                            } else goto lbl_end0;
                        } else {
                            if (--e.nested != 1) {
                                ++i;
                                continue;
                            }
                        }
                    }
                    e.target->end(listener);
                lbl_end0:;
                    stack.erase(stack.begin() + i);
                    if (stack.empty()) break;
                    --i;
                }
            }

        }
    }
}

#endif
