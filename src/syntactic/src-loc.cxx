#include <cstdio>

#include "zephyr-lang/syntactic/src-loc.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            int source_pos_t::print(char * buffer, unsigned long long bufferSize) const noexcept {
                return std::snprintf(buffer, bufferSize, "%lli:%lli",
                    (signed long long)line, (signed long long)column
                );
            }

            int source_loc_t::print(char * buffer, unsigned long long bufferSize) const noexcept {
                return std::snprintf(buffer, bufferSize, "%lli:%lli-%lli:%lli",
                    (signed long long)begin.line, (signed long long)begin.column,
                    (signed long long)end.line, (signed long long)end.column
                );
            }

        }
    }
}
