#include <istream>
#include <iostream>

#include "zephyr-lang/syntactic/compilation-unit.hpp"

#include "zephyr-lang/internal/lexer.hxx"
#include "zephyr-lang/internal/parser.hxx"

#include "throw_.hxx"

namespace zephyr {
    namespace lang {

        namespace syntactic {

            void errors::onError(const std::string & msg, const source_loc_t & l) {
                char loc[64];
                l.print(loc, sizeof(loc));
                std::cerr << msg << " (" << loc << ")" << std::endl;
            }

            CompilationUnitPtr CompilationUnit::parse(std::istream & in, strings & ss, errors & ec) {
                internal::lexer::data_t lexerData;
                lexerData.in = &in;
                lexerData.strings = &ss;
                auto lexer = internal::lexer::alloc(&lexerData);
                if (!lexer) throw std::bad_alloc();
                struct lexer_dtor_t {
                    void * lexer;
                    ~lexer_dtor_t() { internal::lexer::destroy(lexer); }
                } lexer_dtor { lexer };

                CompilationUnitPtr result;
                internal::parser p { lexer, &result, &ec };
#if YYDEBUG && 0
                p.set_debug_level(1);
                p.set_debug_stream(std::cerr);
#endif
                ASSERT(p.parse() == 0, assert_, "failed to parse");

                return result;
            }

            CompilationUnitPtr CompilationUnit::parse(std::istream & s) {
                strings ss;
                errors ec;
                return parse(s, ss, ec);
            }

        }

        namespace internal {

            void parser::error(const typename parser::location_type & loc, const std::string & msg) {
                m_error->onError(msg, loc);
            }

        }

    }
}
