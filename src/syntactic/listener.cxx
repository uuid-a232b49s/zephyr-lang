#include "listener.hxx"
#include "zephyr-lang/syntactic/listener.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            void listen(Listener & listener, Listenable & listenable) {
                return internal::listen<details::ListenableList>(listener, listenable);
            }

        }
    }
}
