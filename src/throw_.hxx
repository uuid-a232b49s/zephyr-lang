#ifndef HEADER_ZEPHYRLANG_INTERNAL_THROW
#define HEADER_ZEPHYRLANG_INTERNAL_THROW 1

#include <string>
#include <utility>

#include "zephyr-lang/exception.hpp"

namespace zephyr {
    namespace lang {
        namespace internal {

            const char * trim_file_name(const char *) noexcept;

            template<typename a>
            struct c_str {
                static const char * x(const a & b) noexcept { return b; }
            };
            template<>
            struct c_str<std::string> {
                static const char * x(const std::string & b) noexcept { return b.c_str(); }
            };

            template<typename exc_t, typename msg_t>
            class exception_impl : public exc_t {
            public:
                msg_t m_msg;
#ifdef ZEPHYRLANG_CURRENT_FUNC
                const char * m_func;
#endif
                const char * m_file;
                signed long long m_line;

                exception_impl(
                    msg_t msg,
#ifdef ZEPHYRLANG_CURRENT_FUNC
                    const char * func,
#endif
                    const char * file,
                    signed long long line
                ) :
                    m_msg(std::move(msg)),
#ifdef ZEPHYRLANG_CURRENT_FUNC
                    m_func(func),
#endif
                    m_file(trim_file_name(file)),
                    m_line(line)
                {}

                virtual char const * what() const noexcept override { return c_str<msg_t>::x(m_msg); }
                virtual const char * file() const noexcept override { return m_file; }
                virtual signed long long line() const noexcept override { return m_line; }
                virtual const char * func() const noexcept override
#ifdef ZEPHYRLANG_CURRENT_FUNC
                { return m_func; }
#else
                { return ""; }
#endif

            };

            template<typename>
            struct get_exc/* {
                template<typename ex> using t = exception_impl<ex, std::string>;
            }*/;
            template<>
            struct get_exc<const char *> {
                template<typename ex> using t = exception_impl<ex, const char *>;
            };
            template<unsigned n>
            struct get_exc<const char (&) [n]> : get_exc<const char *> {};
            template<unsigned n>
            struct get_exc<const char [n]> : get_exc<const char *> {};

        }
    }
}

#ifdef ZEPHYRLANG_CURRENT_FUNC
#define THROW0(exc, msg) (throw typename ::zephyr::lang::internal::get_exc<decltype(msg)>::t<::zephyr::lang::exc>(msg, ZEPHYRLANG_CURRENT_FUNC, __FILE__, __LINE__))
#else
#define THROW0(exc, msg) (throw ::zephyr::lang::internal::exception_impl<::zephyr::lang:: exc >(msg, __FILE__, __LINE__))
#endif

#define THROW(exc, msg) THROW0(exc ## exception, msg)
#define THROW_ICE(msg) THROW0(internal_compiler_error, msg)

#define ASSERT(cnd, exc, msg) ( (!!(cnd)) ? ((void)0) : (THROW(exc, ((msg) ? (msg) : #cnd ) )) )
#define ASSERT_ICE(cnd, msg) ( (!!(cnd)) ? ((void)0) : (THROW_ICE(((msg) ? (msg) : #cnd ) )) )

#endif
