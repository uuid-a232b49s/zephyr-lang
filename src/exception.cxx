#include <cstdio>
#include <cstring>

#include "zephyr-lang/exception.hpp"

namespace zephyr {
    namespace lang {

        bool exception::reports_func() noexcept {
            return
#ifdef ZEPHYRLANG_CURRENT_FUNC
                true
#else
                false
#endif
                ;
        };

        int exception::print(char * buffer, unsigned long long bufferSize) const noexcept {
            return std::snprintf(
                buffer, bufferSize,
#ifdef ZEPHYRLANG_CURRENT_FUNC
                "%s at <%s> [%s:%lli]",
                what(), func(), file(), line()
#else
                "%s at [%s:%lli]",
                what(), file(), line()
#endif
            );
        }

        namespace internal {
            const char * trim_file_name(const char * f) noexcept {
                if (!f || f[0] == 0) return "";
                for (auto s = f + std::strlen(f) - 1; s != f; --s) {
                    if (*s == '/' || *s == '\\') {
                        if (
                            std::strncmp(s + 1, "src", 3) == 0 ||
                            std::strncmp(s + 1, "generated", 9) == 0
                        ) {
                            return s + 1;
                        }
                    }
                }
                return f;
            }
        }

    }
}
