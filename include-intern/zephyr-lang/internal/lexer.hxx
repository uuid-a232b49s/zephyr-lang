#ifndef HEADER_ZEPHYRLANG_INTERNAL_LEXER
#define HEADER_ZEPHYRLANG_INTERNAL_LEXER 1

#include <string>
#include <vector>
#include <istream>

#include "zephyr-lang/strings.hpp"

namespace zephyr {
    namespace lang {
        namespace internal {
            namespace lexer {

                struct data_t {
                    std::istream * in = nullptr;
                    std::string lastComment;
                    std::string buffer;
                    std::vector<int> states;
                    lang::strings * strings = nullptr;
                };

                void * alloc(data_t *);
                void destroy(void *);

            }
        }
    }
}

#endif
