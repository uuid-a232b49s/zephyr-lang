#include <iostream>
#include "zephyr-lang/exception.hpp"

#ifndef test0
#define test0(...) test()
#endif

int main(int argc, char * * argv) {
    try {
        return test0(argc, argv);
    } catch (const zephyr::lang::exception & ex) {
        char buff[256];
        ex.print(buff, sizeof(buff));
        std::cerr << buff << std::endl;
    } catch (const std::exception & ex) {
        std::cerr << ex.what() << std::endl;
    } catch (...) {
        std::cerr << "unknown exception" << std::endl;
    }
    return -1;
}
