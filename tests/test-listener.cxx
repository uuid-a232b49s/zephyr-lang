#include <sstream>
#include <iostream>

#include "zephyr-lang/exception.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/syntactic/namespace.hpp"
#include "zephyr-lang/syntactic/compilation-unit.hpp"

static int test() {
    using namespace zephyr::lang::syntactic;

    std::stringstream ss;
    ss.str(R"(
namespace A0 {
    namespace A1 {
        namespace A2a { }
        namespace A2b {
            namespace A3 { }
        }
    }
}
namespace B0 {
    namespace B1 {
        namespace B2 {
            namespace B3a {
                namespace B4 { }
            }
            namespace B3b { }
        }
    }
}
/* namespace { } */
)");

    {
        auto cu = CompilationUnit::parse(ss);
        struct ListenerImpl : Listener {
            virtual bool begin(CompilationUnit &) {
                std::cerr << "begin " << "cu" << std::endl;
                return true;
            }
            virtual void end(CompilationUnit &) {
                std::cerr << "end " << "cu" << std::endl;
            }
            virtual bool begin(Namespace & x) {
                std::cerr << "begin " << *x.identifier << std::endl;
                return true;
            }
            virtual void end(Namespace & x) {
                std::cerr << "end " << *x.identifier << std::endl;
            }
        } listener;

        listen(listener, *cu);
    }

    return 0;
}

#include "test-main.inl"
