#include <fstream>

#include "zephyr-lang/exception.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/syntactic/namespace.hpp"
#include "zephyr-lang/syntactic/compilation-unit.hpp"

static int test(int argc, char * * argv) {
    using namespace zephyr::lang::syntactic;

    std::ifstream in { argv[1] };
    if (!in) return -1;

    {
        auto cu = CompilationUnit::parse(in);
    }

    return 0;
}

#define test0(...) test(__VA_ARGS__)
#include "test-main.inl"
