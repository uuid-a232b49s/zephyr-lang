%require "3.6"

%language "c++"
%locations
%no-lines

//%glr-parser
//%skeleton "glr2.cc"

%define parse.trace
%define parse.error detailed
//%define parse.lac full
//%define api.pure full
%define api.namespace { zephyr::lang::internal }
%define api.value.type variant
%define api.value.automove
%define api.token.constructor
%define api.token.raw

%code requires { #include "zephyr-lang/syntactic/src-loc.hpp" }
%define api.location.type { zephyr::lang::syntactic::source_loc_t }

%param { void * lexer }
%parse-param { U<S::CompilationUnit> * m_output }
%parse-param { S::errors * m_error }

%code requires {
#include <memory>

#include "zephyr-lang/syntactic.hpp"

namespace S = zephyr::lang::syntactic;
using sstr = zephyr::lang::sstr;

template<typename x>
using U = std::unique_ptr<x>;

template<typename x, typename ... args_t>
inline U<x> NEW(args_t && ... args) { return U<x>(new x(std::forward<args_t>(args)...)); }

namespace zephyr { namespace lang { namespace internal { namespace parser_ {
    template<typename base_t, typename target_t>
    struct TC_t {
        target_t value;
        TC_t() = default;
        TC_t(target_t && v) : value(std::move(v)) {}
    };
} } } }
#define TC(baseType, field) ::zephyr::lang::internal::parser_::TC_t<baseType, decltype(std::declval<baseType>().field)>

#define MV std::move

#ifdef _MSC_VER
#pragma warning(disable: 4065)
#endif

}

%code {

#include <cstdlib>

typename zephyr::lang::internal::parser::symbol_type zephyr_lang_yylex0(void * yyscanner);
#define yylex(lexer) zephyr_lang_yylex0(lexer)

template<typename X> struct delete_t { static void d(const X &) noexcept {} };
template<typename X> struct delete_t<X *> { static void d(X * x) noexcept(noexcept(delete std::declval<X *>())) { delete x; } };
template<typename X> static void delete0(X x) noexcept(noexcept(delete_t<X>::d(std::declval<X>()))) { delete_t<X>::d(x); }

using location_t = typename zephyr::lang::internal::parser::location_type;
using position_t = decltype(location_t::begin);

template <typename YYChar>
static std::basic_ostream<YYChar> & operator<< (std::basic_ostream<YYChar> & ostr, const position_t & pos) {
    char buff[64];
    pos.print(buff, sizeof(buff));
    return ostr;
}

template <typename YYChar>
static std::basic_ostream<YYChar> & operator<< (std::basic_ostream<YYChar> & ostr, const location_t & loc) {
    char buff[128];
    loc.print(buff, sizeof(buff));
    return ostr;
}

}

%start entry

%%

%token <signed long long> K_int ;
%token <unsigned long long> K_uint ;
%token <double> K_flt ;

%right K_eq ;

%left K_eqeq K_ls K_gr K_lse K_gre ;

%token <sstr> K_str K_identifier ;
%token K_namespace K_lambda K_if K_else K_return ;
%token <signed long long> K_id_int K_id_uint ;
%token K_id_auto K_id_flt K_id_const K_id_stack K_id_ref K_id_void ;
%token K_swbr_l K_swbr_r K_sqbr_l K_sqbr_r K_par_l K_par_r ;
%token K_coloncolon K_colon K_scolon K_coma ;

%left K_add K_sub ;
%left K_mul K_div ;

%precedence PREC_INVOKE ;

%precedence PREC_NEG ;

%right K_deref ;
%left K_fstop ;



entry : compilationUnit { *m_output = $1; } ;

compilationUnit
    : %empty { $$ = NEW<S::CompilationUnit>(); }
    | cuDeclaration.list { $$ = NEW<S::CompilationUnit>($1); }
; %nterm <U<S::CompilationUnit>> compilationUnit;



cuDeclaration
    : declNamespace { $$ = $1; }
    | declFunction { $$ = $1; }
; %nterm <U<S::CuDeclaration>> cuDeclaration;
cuDeclaration.list
    : cuDeclaration { $$.emplace_back($1); }
    | cuDeclaration.list cuDeclaration { $$ = $1; $$.emplace_back($2); }
; %nterm <S::CuDeclarationList> cuDeclaration.list;

declNamespace
    : K_namespace K_identifier K_swbr_l K_swbr_r { $$ = NEW<S::Namespace>($2, decltype(S::Namespace::declarations)()); }
    | K_namespace K_identifier[i] K_swbr_l cuDeclaration.list[l] K_swbr_r { $$ = NEW<S::Namespace>(); $$->identifier = $i; $$->declarations = $l; }
; %nterm <U<S::Namespace>> declNamespace;



qualTypeName
    : qualTypeName.isGlobal qualTypeName.list { $$ = NEW<S::QualifiedTypeName>(MV($2.value), $1); }
; %nterm <U<S::QualifiedTypeName>> qualTypeName;
qualTypeName.isGlobal
    : %empty { $$ = false; }
    | K_coloncolon { $$ = true; }
; %nterm <bool> qualTypeName.isGlobal;
qualTypeName.list
    : K_identifier { $$.value.emplace_back($1); }
    | qualTypeName.list K_coloncolon K_identifier { $$ = $1; $$.value.emplace_back($3); }
; %nterm <TC(S::QualifiedTypeName, value)> qualTypeName.list;

primitiveTypeName
    : K_id_int { $$ = NEW<S::PrimitiveTypeName>(S::PrimitiveTypeName::kind_t::int_); }
    | K_id_uint { $$ = NEW<S::PrimitiveTypeName>(S::PrimitiveTypeName::kind_t::uint_); }
    | K_id_flt { $$ = NEW<S::PrimitiveTypeName>(S::PrimitiveTypeName::kind_t::float_); }
    | K_id_void { $$ = NEW<S::PrimitiveTypeName>(S::PrimitiveTypeName::kind_t::void_); }
; %nterm <U<S::PrimitiveTypeName>> primitiveTypeName;

typeMeta
    : typeMeta.refInfo typeMeta.typeName {
        $$.refInfo = $1;
        $$.typeName = $2;
    }
; %nterm <S::TypeMeta> typeMeta;
typeMeta.typeName
    : qualTypeName { $$ = $1; }
    | primitiveTypeName { $$ = $1; }
    | K_id_auto { $$ = NEW<S::AutoTypeName>(); }
; %nterm <U<S::TypeName>> typeMeta.typeName;
typeMeta.refInfo
    : %empty {}
    | typeMeta.refInfo.isConst typeMeta.refInfo.isStack K_id_ref { $$ = S::RefInfo(true, $1, $2); }
; %nterm <S::RefInfo> typeMeta.refInfo;
typeMeta.refInfo.isConst : %empty { $$ = false; } | K_id_const { $$ = true; } ; %nterm <bool> typeMeta.refInfo.isConst;
typeMeta.refInfo.isStack : %empty { $$ = false; } | K_id_stack { $$ = true; } ; %nterm <bool> typeMeta.refInfo.isStack;



declFunction
    : typeMeta[r] K_identifier[i] K_par_l declFunction.argList[a] K_par_r stmtScope[b] {
        $$ = NEW<S::Function>($i, MV($a.value), $r, $b);
    }
; %nterm <U<S::Function>> declFunction;
declFunction.argList
    : %empty {}
    | declFunction.argList.0 { $$ = MV($1.value); }
; %nterm <TC(S::Function, args)> declFunction.argList;
declFunction.argList.0
    : declFunction.argList.1 { $$.value.emplace_back($1); }
    | declFunction.argList.0 K_coma declFunction.argList.1 { $$ = $1; $$.value.emplace_back($3); }
; %nterm <TC(S::Function, args)> declFunction.argList.0;
declFunction.argList.1
    : typeMeta { $$.typeMeta = $1; }
    | typeMeta K_identifier { $$ = S::FunctionArg($2, $1); }
; %nterm <S::FunctionArg> declFunction.argList.1;



stmt
    : stmtScope { $$ = $1; }
    | typeMeta K_identifier K_scolon { $$ = NEW<S::stmt::DeclLocal>($2, $1); }
    | typeMeta K_identifier K_eq expr K_scolon { $$ = NEW<S::stmt::DeclLocalInit>($2, $1, $4); }
    | K_if K_par_l expr[c] K_par_r stmt[t] { $$ = NEW<S::stmt::If>($c, $t); }
    | K_if K_par_l expr[c] K_par_r stmt[t] K_else stmt[f] { $$ = NEW<S::stmt::IfElse>($c, $t, $f); }
    | expr K_scolon { $$ = NEW<S::stmt::Discard>($1); }
    | K_return K_scolon { $$ = NEW<S::stmt::Return>(); }
    | K_return expr K_scolon { $$ = NEW<S::stmt::ReturnValue>($2); }
; %nterm <U<S::stmt::Statement>> stmt;

stmtScope
    : K_swbr_l stmtScope.raw K_swbr_r { $$ = NEW<S::stmt::Scope>(MV($2.value)); }
    | K_swbr_l K_swbr_r { $$ = NEW<S::stmt::Scope>(); }
; %nterm <U<S::stmt::Scope>> stmtScope;
stmtScope.raw
    : stmt { $$.value.emplace_back($1); }
    | stmtScope.raw stmt { $$ = $1; $$.value.emplace_back($2); }
; %nterm <TC(S::stmt::Scope, statements)> stmtScope.raw;



expr
    : K_par_l expr K_par_r { $$ = $2; }
    | exprLiteral { $$ = $1; }
    | exprMbrAccess { $$ = $1; }
    | K_identifier { $$ = NEW<S::expr::Identifier>($1); }
    | expr K_eq expr { $$ = NEW<S::expr::Assign>($1, $3); }
    | expr K_add expr { $$ = NEW<S::expr::Binary>($1, $3, S::expr::Binary::op_t::add); }
    | expr K_sub expr { $$ = NEW<S::expr::Binary>($1, $3, S::expr::Binary::op_t::sub); }
    | expr K_mul expr { $$ = NEW<S::expr::Binary>($1, $3, S::expr::Binary::op_t::mul); }
    | expr K_div expr { $$ = NEW<S::expr::Binary>($1, $3, S::expr::Binary::op_t::div); }
    | expr K_eqeq expr { $$ = NEW<S::expr::Compare>($1, $3, S::expr::Compare::op_t::eq); }
    | expr K_ls expr { $$ = NEW<S::expr::Compare>($1, $3, S::expr::Compare::op_t::ls); }
    | expr K_gr expr { $$ = NEW<S::expr::Compare>($1, $3, S::expr::Compare::op_t::gr); }
    | expr K_lse expr { $$ = NEW<S::expr::Compare>($1, $3, S::expr::Compare::op_t::lse); }
    | expr K_gre expr { $$ = NEW<S::expr::Compare>($1, $3, S::expr::Compare::op_t::gre); }
    | %prec PREC_INVOKE expr[t] K_par_l expr.argList[a] K_par_r { $$ = NEW<S::expr::Invoke>($t, $a.value); }
    | %prec PREC_NEG K_sub expr { $$ = NEW<S::expr::Unary>($2, S::expr::Unary::op_t::neg); }
    | K_sqbr_l K_sqbr_r K_par_l declFunction.argList[a] K_par_r stmtScope[b] { $$ = NEW<S::expr::Lambda>(MV($a.value), $b); }
; %nterm <U<S::expr::Expression>> expr;

expr.argList
    : %empty {}
    | expr.argList.e { $$ = $1; }
; %nterm <TC(S::expr::Invoke, args)> expr.argList;
expr.argList.e
    : expr { $$.value.emplace_back($1); }
    | expr.argList.e K_coma expr { $$ = $1; $$.value.emplace_back($3); }
; %nterm <TC(S::expr::Invoke, args)> expr.argList.e;

exprLiteral
    : K_int { $$ = NEW<S::expr::Literal>($1); }
    | K_uint { $$ = NEW<S::expr::Literal>($1); }
    | K_flt { $$ = NEW<S::expr::Literal>($1); }
; %nterm <U<S::expr::Literal>> exprLiteral;

exprMbrAccess
    : typeMeta.typeName K_fstop K_identifier { { $$ = NEW<S::expr::MemberAccess>(); $$->fields.emplace_back($3); } }
    | typeMeta.typeName K_fstop exprMbrAccess.instance { $$ = NEW<S::expr::MemberAccess>(); $$->type = $1; $$->fields = std::move($3.value); }
    | exprMbrAccess.instance { $$ = NEW<S::expr::MemberAccess>(); $$->fields = std::move($1.value); }
; %nterm <U<S::expr::MemberAccess>> exprMbrAccess;
exprMbrAccess.instance
    : K_identifier K_fstop K_identifier { $$.value.emplace_back($1); $$.value.emplace_back($3); }
    | exprMbrAccess.instance K_fstop K_identifier { $$.value.emplace_back($3); }
; %nterm <TC(S::expr::MemberAccess, fields)> exprMbrAccess.instance;

%%
