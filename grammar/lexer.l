%option noyywrap nounput noinput reentrant yylineno nounistd never-interactive

%top {
    #ifdef _MSC_VER
    #pragma warning(disable: 4005)
    #pragma warning(disable: 4267)
    #endif

    #include "zephyr-lang/internal/lexer.hxx"
    #define YY_EXTRA_TYPE zephyr::lang::internal::lexer::data_t *

    #define YY_INPUT(buf, result, max_size) { \
    yyextra->in->read( (buf) , (max_size) ); \
    (result) = static_cast<decltype(result)>(yyextra->in->gcount()); }
}

%{
    #include <cstdlib>
    #include <cstring>
   
    #include "parser.hxx"
    #include "throw_.hxx"

    static signed long long parseLL(const char * s, int base = 10) {
        using namespace std;
        char * e;
        auto v = strtoll(s, &e, base);
        ASSERT(strlen(s) == (e - s), lexer_, "failed to parse integer literal");
        return v;
    }

    static double parseDBL(const char * s) {
        using namespace std;
        char * e;
        auto v = strtod(s, &e);
        ASSERT(strlen(s) == (e - s), lexer_, "failed to parse floating literal");
        return v;
    }

    using P = zephyr::lang::internal::parser;

    using loc_t = typename P::location_type;
    using pos_t = decltype(loc_t::begin);
    #define L() loc_t { pos_t { yylineno, yycolumn }, pos_t { yylineno, yycolumn } }

    #define YY_DECL typename P::symbol_type zephyr_lang_yylex0(yyscan_t yyscanner)

    #define YY_FATAL_ERROR(msg) THROW(lexer_, msg)

    //#define YY_USER_ACTION
    //#define YY_BREAK break;
    #define YY_USER_ACTION yycolumn += std::strlen(yytext);

%}

DIGIT   [0-9]

%x COMMENT1
%x COMMENT0
%x STRING

%%

%{
    auto & states = yyextra->states;
    auto beginState = [&] (decltype(YYSTATE) s) {
        states.emplace_back(YYSTATE);
        BEGIN(s);
    };
    auto restoreState = [&] {
        ASSERT(!states.empty(), lexer_, "failed to restore lexer sub-state");
        BEGIN(states.back());
        states.pop_back();
    };

    auto & strBuff = yyextra->buffer;
    auto appendStrBuff = [&] (const char * begin, const char * end = nullptr) {
        if (end) {
            strBuff.insert(strBuff.end(), begin, end);
        } else {
            strBuff.append(begin);
        }
    };
%}

[[:space:]]+                {}

"/*"                        {
                                strBuff.clear();
                                beginState(COMMENT1);
                            }
"\""                        {
                                strBuff.clear();
                                beginState(STRING);
                            }

"//"                        {
                                strBuff.clear();
                                beginState(COMMENT0);
                            }

"["                         { return P::make_K_sqbr_l(L()); }
"]"                         { return P::make_K_sqbr_r(L()); }
"{"                         { return P::make_K_swbr_l(L()); }
"}"                         { return P::make_K_swbr_r(L()); }
"("                         { return P::make_K_par_l(L()); }
")"                         { return P::make_K_par_r(L()); }
"namespace"                 { return P::make_K_namespace(L()); }
"lambda"                    { return P::make_K_lambda(L()); }
"if"                        { return P::make_K_if(L()); }
"else"                      { return P::make_K_else(L()); }
"return"                    { return P::make_K_return(L()); }
"::"                        { return P::make_K_coloncolon(L()); }
":"                         { return P::make_K_colon(L()); }
";"                         { return P::make_K_scolon(L()); }
"."                         { return P::make_K_fstop(L()); }
","                         { return P::make_K_coma(L()); }
"=="                        { return P::make_K_eqeq(L()); }
"="                         { return P::make_K_eq(L()); }
">="                        { return P::make_K_gre(L()); }
"<="                        { return P::make_K_lse(L()); }
">"                         { return P::make_K_gr(L()); }
"<"                         { return P::make_K_ls(L()); }
"auto"                      { return P::make_K_id_auto(L()); }
"u"?"int"(?:[[:digit:]]+)?  {
                                return
                                    yytext[0] == 'u' ?
                                    P::make_K_id_uint(parseLL(yytext + 4), L()) :
                                    P::make_K_id_int(parseLL(yytext + 3), L());
                            }
"usize"                     { return P::make_K_id_uint(0, L()); }
"isize"                     { return P::make_K_id_int(0, L()); }
"flt"(?:[[:digit:]]+)?      { return P::make_K_id_flt(L()); }
"void"                      { return P::make_K_id_void(L()); }
"deref"                     { return P::make_K_deref(L()); }
"ref"                       { return P::make_K_id_ref(L()); }
"const"                     { return P::make_K_id_const(L()); }

"-"?[[:digit:]]+"."[[:digit:]]+ { return P::make_K_flt(parseDBL(yytext), L()); }
"-"?[[:digit:]]+                { return P::make_K_int(parseLL(yytext, 10), L()); }

"-"                         { return P::make_K_sub(L()); }
"+"                         { return P::make_K_add(L()); }
"/"                         { return P::make_K_div(L()); }
"*"                         { return P::make_K_mul(L()); }

[_[:alpha:]][_[:alnum:]]*   { return P::make_K_identifier(yyextra->strings->get(yytext), L()); }

.                           {
                                //throw P::syntax_error(L(), "unexpected input");
                                return P::make_YYUNDEF(L());
                            }

<STRING>"\\\""              {
                                appendStrBuff(yytext, yytext + yyleng - 2);
                                strBuff += '\"';
                            }
<STRING>"\""                {
                                appendStrBuff(yytext, yytext + yyleng - 1);
                                restoreState();
                                return P::make_K_str(yyextra->strings->get(strBuff.c_str()), L());
                            }
<STRING>.                   { appendStrBuff(yytext, yytext + yyleng); /*yymore();*/ }

<COMMENT1>[^*\n]*            { appendStrBuff(yytext); }
<COMMENT1>"*"+[^*/\n]*       { appendStrBuff(yytext + 1); }
<COMMENT1>\n                 { appendStrBuff(yytext); }
<COMMENT1><<EOF>>            { return P::make_YYEOF(L()); }
<COMMENT1>"*"+"/"            {
                                std::swap(yyextra->lastComment, strBuff);
                                restoreState();
                            }

<COMMENT0>[^\n]             { appendStrBuff(yytext); }
<COMMENT0>\n                {
                                std::swap(yyextra->lastComment, strBuff);
                                restoreState();
                            }
<COMMENT0><<EOF>>           { return P::make_YYEOF(L()); }

<<EOF>>                     { return P::make_YYEOF(L()); }

%%

#include <istream>

namespace {
    static inline void _unused_fns() {
        (void)(&yy_fatal_error);
    }
}

namespace zephyr { namespace lang { namespace internal { namespace lexer {

    void * alloc(data_t * data) {
        yyscan_t s;
        yylex_init(&s);
        if (!s) return nullptr;
        yylex_init_extra(data, &s);
        return static_cast<void *>(s);
    }

    void destroy(void * yyscanner) {
        if (!yyscanner) return;
        yylex_destroy(static_cast<yyscan_t>(yyscanner));

        (void)(&_unused_fns);
    }

} } } }
