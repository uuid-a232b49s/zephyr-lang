#ifndef HEADER_ZEPHYRLANG_EXCEPTION
#define HEADER_ZEPHYRLANG_EXCEPTION 1

#include <exception>

#include "zephyr-lang/internal/api.hpp"

namespace zephyr {
    namespace lang {

        class ZEPHYRLANG_API exception : public std::exception {
        public:
            virtual signed long long line() const noexcept = 0;
            virtual const char * file() const noexcept = 0;
            virtual const char * func() const noexcept = 0;
            static bool reports_func() noexcept;
            virtual int print(char * buffer, unsigned long long bufferSize) const noexcept;
        };

        class ZEPHYRLANG_API assert_exception : public exception {};
        class ZEPHYRLANG_API internal_compiler_error : public exception {};
        class ZEPHYRLANG_API lexer_exception : public exception {};

    }
}

#endif
