#ifndef HEADER_ZEPHYRLANG_SEMANTIC_TYPE
#define HEADER_ZEPHYRLANG_SEMANTIC_TYPE 1

#include "../strings.hpp"

#include "module.hpp"
#include "zephyr-lang/semantic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace semantic {

            struct ZEPHYRLANG_API Type : Listenable {
                ZEPHYRLANG_DECL_CTOR_I(Type);
            };

            struct ZEPHYRLANG_API PrimitiveType : Type {
                enum struct kind_t {
                    undefined,
                    void_,
                    int_,
                    uint_,
                    float_
                } kind = kind_t::undefined;
                unsigned width = 0;

                explicit PrimitiveType(kind_t kind = kind_t::undefined, decltype(width) width = 0) noexcept : kind(kind), width(width) {}

            }; ZEPHYRLANG_LISTEN_TYPE(PrimitiveType);

        }
    }
}

#endif
