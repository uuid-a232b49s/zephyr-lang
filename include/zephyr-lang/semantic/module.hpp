#ifndef HEADER_ZEPHYRLANG_SEMANTIC_MODULE
#define HEADER_ZEPHYRLANG_SEMANTIC_MODULE 1

#include <memory>
#include <vector>
#include <string>

#include "zephyr-lang/semantic/listener.hpp"
#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            struct CompilationUnit;
        }
    }
}

namespace zephyr {
    namespace lang {
        namespace semantic {

            struct ZEPHYRLANG_API CuContainer {
                virtual syntactic::CompilationUnit & at(unsigned long long) = 0;
                virtual unsigned long long count() = 0;
            };
            struct CuVector : CuContainer, std::vector<std::unique_ptr<syntactic::CompilationUnit>> {
                virtual syntactic::CompilationUnit & at(unsigned long long) override;
                virtual unsigned long long count() override;
            };

            struct ZEPHYRLANG_API Global : Listenable {
                ZEPHYRLANG_DECL_CTOR_I(Global);
            };

            struct ZEPHYRLANG_API errors {
                virtual void onError(const std::string & msg, syntactic::Listenable & targetA, semantic::Listenable & targetB) {}
            };

            struct ZEPHYRLANG_API Module : Global {
                std::vector<std::unique_ptr<Global>> declarations;

                ZEPHYRLANG_DECL_CTOR_D(Module);
                ZEPHYRLANG_DECL_NOCOPY(Module);
                ZEPHYRLANG_SEMANTIC_LISTENER_impl(for (auto & x : declarations) ll.add(*x));

                static std::unique_ptr<Module> assemble(CuContainer &, errors &);

            }; ZEPHYRLANG_LISTEN_TYPE(Module);

        }
    }
}

#endif
