#ifndef HEADER_ZEPHYRLANG_SEMANTIC_NAMESPACE
#define HEADER_ZEPHYRLANG_SEMANTIC_NAMESPACE 1

#include <memory>
#include <vector>

#include "module.hpp"
#include "zephyr-lang/semantic/listener.hpp"

#include "../strings.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            struct CompilationUnit;
        }
    }
}

namespace zephyr {
    namespace lang {
        namespace semantic {

            struct ZEPHYRLANG_API Namespace : Global {
                Global * container = nullptr;
                sstr identifier;
                std::vector<std::unique_ptr<Global>> declarations;

                ZEPHYRLANG_DECL_CTOR_D(Namespace);
                ZEPHYRLANG_DECL_NOCOPY(Namespace);

                ZEPHYRLANG_SEMANTIC_LISTENER_impl(for (auto & d : declarations) ll.add(*d))

            }; ZEPHYRLANG_LISTEN_TYPE(Namespace);

        }
    }
}

#endif
