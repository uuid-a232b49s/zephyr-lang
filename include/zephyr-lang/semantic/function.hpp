#ifndef HEADER_ZEPHYRLANG_SEMANTIC_FUNCTION
#define HEADER_ZEPHYRLANG_SEMANTIC_FUNCTION 1

#include "module.hpp"

#include "../strings.hpp"

#include "zephyr-lang/semantic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace semantic {

            struct ZEPHYRLANG_API Function : Global {
                sstr identifier;
                Global * container = nullptr;

                ZEPHYRLANG_DECL_CTOR_D(Function);
                ZEPHYRLANG_DECL_NOCOPY(Function);
                ZEPHYRLANG_SEMANTIC_LISTENER_impl();

            }; ZEPHYRLANG_LISTEN_TYPE(Function);

        }
    }
}

#endif
