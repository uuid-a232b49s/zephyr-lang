#ifndef HEADER_ZEPHYRLANG_STRINGS
#define HEADER_ZEPHYRLANG_STRINGS 1

#include <memory>
#include <string>
#include <unordered_map>

#include "zephyr-lang/internal/api.hpp"

namespace zephyr {
    namespace lang {

        using sstr = std::shared_ptr<const std::string>;

        struct ZEPHYRLANG_API strings {
        private:
            std::unordered_map<const char *, sstr> m_strings;

        public:

            sstr get(const char *);

            sstr operator()(const char * s) { return get(s); }

        };

    }
}

#endif
