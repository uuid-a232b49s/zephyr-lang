#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_NAMESPACE
#define HEADER_ZEPHYRLANG_SYNTACTIC_NAMESPACE 1

#include "../strings.hpp"
#include "compilation-unit.hpp"
#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            struct ZEPHYRLANG_API Namespace : CuDeclaration {
                sstr identifier;
                CuDeclarationList declarations;

                ZEPHYRLANG_DECL_CTOR_2(Namespace, identifier, declarations);
                ZEPHYRLANG_DECL_CTOR_D(Namespace);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl(for (auto & x : declarations) ll.add(*x));

            }; ZEPHYRLANG_LISTEN_TYPE(Namespace);

        }
    }
}

#endif
