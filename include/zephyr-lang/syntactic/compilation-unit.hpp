#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_COMPILATIONUNIT
#define HEADER_ZEPHYRLANG_SYNTACTIC_COMPILATIONUNIT 1

#include <memory>
#include <vector>
#include <string>
#include <istream>

#include "../strings.hpp"
#include "src-loc.hpp"
#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            struct ZEPHYRLANG_API CuDeclaration : Listenable {
                ZEPHYRLANG_DECL_CTOR_I(CuDeclaration);
            };
            using CuDeclarationList = std::vector<std::unique_ptr<CuDeclaration>>;

            struct ZEPHYRLANG_API errors {
                virtual void onError(const std::string & msg, const source_loc_t &);
            };

            struct CompilationUnit;
            using CompilationUnitPtr = std::unique_ptr<CompilationUnit>;

            struct ZEPHYRLANG_API CompilationUnit : Listenable {
                CuDeclarationList declarations;

                ZEPHYRLANG_DECL_CTOR_1(CompilationUnit, declarations);
                ZEPHYRLANG_DECL_CTOR_D(CompilationUnit);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl(for (auto & x : declarations) ll.add(*x));

                static CompilationUnitPtr parse(std::istream &);
                static CompilationUnitPtr parse(std::istream &, strings &, errors &);

            }; ZEPHYRLANG_LISTEN_TYPE(CompilationUnit);

        }
    }
}

#endif
