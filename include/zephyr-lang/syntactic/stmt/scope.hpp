#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_SCOPE
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_SCOPE 1

#include <memory>
#include <vector>

#include "statement.hpp"
#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API Scope : Statement {
                    std::vector<std::unique_ptr<Statement>> statements;

                    ZEPHYRLANG_DECL_CTOR_1(Scope, statements);
                    ZEPHYRLANG_DECL_CTOR_D(Scope);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(for (auto & x : statements) ll.add(*x));

                } ZEPHYRLANG_LISTEN_TYPE(stmt::Scope);

            }
        }
    }
}

#endif
