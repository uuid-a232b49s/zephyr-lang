#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_RETURN
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_RETURN 1

#include <memory>

#include "statement.hpp"
#include "../expr/expression.hpp"

#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API Return : Statement {

                    ZEPHYRLANG_DECL_CTOR_D(Return);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl();

                } ZEPHYRLANG_LISTEN_TYPE(stmt::Return);

                struct ZEPHYRLANG_API ReturnValue : Return {
                    std::unique_ptr<expr::Expression> value;

                    ZEPHYRLANG_DECL_CTOR_D(ReturnValue);
                    ZEPHYRLANG_DECL_CTOR_1(ReturnValue, value);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*value));

                } ZEPHYRLANG_LISTEN_TYPE(stmt::ReturnValue);

            }
        }
    }
}

#endif
