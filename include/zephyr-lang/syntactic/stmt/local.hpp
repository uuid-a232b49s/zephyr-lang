#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_LOCAL
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_LOCAL 1

#include <memory>

#include "../../strings.hpp"

#include "../identifier.hpp"
#include "statement.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "../expr/expression.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API DeclLocal : Statement {
                    sstr identifier;
                    TypeMeta type;

                    ZEPHYRLANG_DECL_CTOR_2(DeclLocal, identifier, type);
                    ZEPHYRLANG_DECL_CTOR_D(DeclLocal);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(type));

                } ZEPHYRLANG_LISTEN_TYPE(stmt::DeclLocal);

                struct ZEPHYRLANG_API DeclLocalInit : DeclLocal {
                    std::unique_ptr<expr::Expression> expression;

                    DeclLocalInit(sstr identifier, TypeMeta type, decltype(expression) expression)
                        : DeclLocal(std::move(identifier), std::move(type)), expression(std::move(expression)) {}
                    ZEPHYRLANG_DECL_CTOR_D(DeclLocalInit);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(this->type), ll.add(*expression));

                } ZEPHYRLANG_LISTEN_TYPE(stmt::DeclLocalInit);

            }
        }
    }
}

#endif
