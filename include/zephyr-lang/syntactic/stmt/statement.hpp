#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_STATEMENT
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_STATEMENT 1

#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API Statement : Listenable {
                    ZEPHYRLANG_DECL_CTOR_I(Statement);
                };

            }
        }
    }
}

#endif
