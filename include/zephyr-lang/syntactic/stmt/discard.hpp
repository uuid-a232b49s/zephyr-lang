#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_DISCARD
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_DISCARD 1

#include <memory>

#include "statement.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "../expr/expression.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API Discard : Statement {
                    std::unique_ptr<expr::Expression> expression;

                    ZEPHYRLANG_DECL_CTOR_1(Discard, expression);
                    ZEPHYRLANG_DECL_CTOR_D(Discard);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*expression));

                } ZEPHYRLANG_LISTEN_TYPE(stmt::Discard);

            }
        }
    }
}

#endif
