#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_IF
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_IF 1

#include <memory>
#include <utility>

#include "statement.hpp"
#include "../expr/expression.hpp"

#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API If : Statement {
                    std::unique_ptr<expr::Expression> condition;
                    std::unique_ptr<Statement> onTrue;

                    ZEPHYRLANG_DECL_CTOR_D(If);
                    ZEPHYRLANG_DECL_CTOR_2(If, condition, onTrue);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(
                        ll.add(*condition);
                        ll.add(*onTrue);
                    );

                } ZEPHYRLANG_LISTEN_TYPE(stmt::If);

                struct ZEPHYRLANG_API IfElse : If {
                    std::unique_ptr<Statement> onFalse;

                    ZEPHYRLANG_DECL_CTOR_D(IfElse);
                    IfElse(decltype(If::condition) condition, decltype(If::onTrue) onTrue, decltype(onFalse) onFalse)
                        : If(std::move(condition), std::move(onTrue)), onFalse(std::move(onFalse)) {}

                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(
                        ll.add(*this->condition);
                        ll.add(*this->onTrue);
                        ll.add(*onFalse);
                    );

                };

            }
        }
    }
}

#endif
