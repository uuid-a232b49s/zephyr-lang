#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_STMT_WHILE
#define HEADER_ZEPHYRLANG_SYNTACTIC_STMT_WHILE 1

#include <memory>
#include <utility>

#include "statement.hpp"
#include "local.hpp"
#include "../expr/expression.hpp"

#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace stmt {

                struct ZEPHYRLANG_API For : Statement {
                    std::unique_ptr<DeclLocalInit> init;
                    std::unique_ptr<expr::Expression> condition;
                    std::unique_ptr<expr::Expression> increment;

                    ZEPHYRLANG_DECL_CTOR_D(For);
                    ZEPHYRLANG_DECL_CTOR_3(For, init, condition, increment);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(
                        ll.add(*init);
                        ll.add(*condition);
                        ll.add(*increment);
                    );

                } ZEPHYRLANG_LISTEN_TYPE(stmt::For);

                struct ZEPHYRLANG_API While : Statement {
                    std::unique_ptr<expr::Expression> condition;

                    ZEPHYRLANG_DECL_CTOR_D(While);
                    ZEPHYRLANG_DECL_CTOR_1(While, condition);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*condition));

                } ZEPHYRLANG_LISTEN_TYPE(stmt::While);

            }
        }
    }
}

#endif
