#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_SRCLOC
#define HEADER_ZEPHYRLANG_SYNTACTIC_SRCLOC 1

#include "zephyr-lang/internal/api.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            using source_line_t = signed long long;
            struct ZEPHYRLANG_API source_pos_t {
                source_line_t line, column;
                int print(char * buffer, unsigned long long bufferSize) const noexcept;
            };
            struct ZEPHYRLANG_API source_loc_t {
                source_pos_t begin, end;
                int print(char * buffer, unsigned long long bufferSize) const noexcept;
            };

        }
    }
}

#endif
