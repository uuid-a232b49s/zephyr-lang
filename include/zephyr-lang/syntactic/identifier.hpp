#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_IDENTIFIER
#define HEADER_ZEPHYRLANG_SYNTACTIC_IDENTIFIER 1

#include <memory>
#include <vector>

#include "../strings.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            struct ZEPHYRLANG_API TypeName : Listenable {
                ZEPHYRLANG_DECL_CTOR_I(TypeName);
            };

            struct ZEPHYRLANG_API QualifiedTypeName : TypeName {
                std::vector<sstr> value;
                bool isGlobal = false;

                ZEPHYRLANG_DECL_CTOR_2(QualifiedTypeName, value, isGlobal);
                ZEPHYRLANG_DECL_CTOR_D(QualifiedTypeName);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl();

            }; ZEPHYRLANG_LISTEN_TYPE(QualifiedTypeName);

            struct ZEPHYRLANG_API PrimitiveTypeName : TypeName {
                enum struct kind_t {
                    undefined,
                    void_,
                    int_,
                    uint_,
                    float_
                } kind = kind_t::undefined;
                unsigned width = 0;

                explicit PrimitiveTypeName(kind_t kind = kind_t::undefined, decltype(width) width = 0) noexcept : kind(kind), width(width) {}

                ZEPHYRLANG_SYNTACTIC_LISTENER_impl();

            }; ZEPHYRLANG_LISTEN_TYPE(PrimitiveTypeName);

            struct ZEPHYRLANG_API AutoTypeName : TypeName {
                ZEPHYRLANG_DECL_CTOR_D(AutoTypeName);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl();
            }; ZEPHYRLANG_LISTEN_TYPE(AutoTypeName);

            struct ZEPHYRLANG_API RefInfo {
                unsigned char isRef : 1;
                unsigned char isConst : 1;
                unsigned char isStack : 1;

                RefInfo() noexcept : RefInfo(false, false, false) {}
                RefInfo(bool isRef, bool isConst, bool isStack) noexcept : isRef(isRef), isConst(isConst), isStack(isStack) {}

            };

            struct ZEPHYRLANG_API TypeMeta : Listenable {
                std::unique_ptr<TypeName> typeName;
                RefInfo refInfo;

                ZEPHYRLANG_DECL_CTOR_2(TypeMeta, typeName, refInfo);
                ZEPHYRLANG_DECL_CTOR_D(TypeMeta);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*typeName));

            }; ZEPHYRLANG_LISTEN_TYPE(TypeMeta);

        }
    }
}

#endif
