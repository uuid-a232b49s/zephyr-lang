#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_COMPARE
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_COMPARE 1

#include <memory>

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Compare : Expression {
                    std::unique_ptr<Expression> a, b;
                    enum struct op_t { null, eq, ls, lse, gr, gre } op = op_t::null;

                    ZEPHYRLANG_DECL_CTOR_3(Compare, a, b, op);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*a); ll.add(*b));

                } ZEPHYRLANG_LISTEN_TYPE(expr::Compare);

            }
        }
    }
}

#endif
