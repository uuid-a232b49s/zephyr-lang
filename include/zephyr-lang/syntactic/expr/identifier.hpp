#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_IDENTIFIER
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_IDENTIFIER 1

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "../../strings.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Identifier : Expression {
                    sstr target;

                    ZEPHYRLANG_DECL_CTOR_1(Identifier, target);
                    ZEPHYRLANG_DECL_CTOR_D(Identifier);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl();

                } ZEPHYRLANG_LISTEN_TYPE(expr::Identifier);

            }
        }
    }
}

#endif
