#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_UNARY
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_UNARY 1

#include <memory>

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Unary : Expression {
                    std::unique_ptr<Expression> a;
                    enum struct op_t { null, neg } op = op_t::null;

                    ZEPHYRLANG_DECL_CTOR_2(Unary, a, op);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*a));

                } ZEPHYRLANG_LISTEN_TYPE(expr::Unary);

            }
        }
    }
}

#endif
