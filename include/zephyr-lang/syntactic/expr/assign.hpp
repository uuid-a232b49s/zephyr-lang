#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_ASSIGN
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_ASSIGN 1

#include <memory>

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Assign : Expression {
                    std::unique_ptr<Expression> target, value;

                    ZEPHYRLANG_DECL_CTOR_2(Assign, target, value);
                    ZEPHYRLANG_DECL_CTOR_D(Assign);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*target); ll.add(*value));

                } ZEPHYRLANG_LISTEN_TYPE(expr::Assign);

            }
        }
    }
}

#endif
