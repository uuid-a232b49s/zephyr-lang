#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_EXPRESSION
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_EXPRESSION 1

#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Expression : Listenable {
                    ZEPHYRLANG_DECL_CTOR_I(Expression);
                };

            }
        }
    }
}

#endif
