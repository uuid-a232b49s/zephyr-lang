#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_INVOKE
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_INVOKE 1

#include <vector>

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "../identifier.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Invoke : Expression {
                    std::unique_ptr<Expression> target;
                    std::vector<std::unique_ptr<Expression>> args;

                    ZEPHYRLANG_DECL_CTOR_2(Invoke, target, args);
                    ZEPHYRLANG_DECL_CTOR_D(Invoke);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*target); for (auto & x : args) { ll.add(*x); });

                } ZEPHYRLANG_LISTEN_TYPE(expr::Invoke);

            }
        }
    }
}

#endif
