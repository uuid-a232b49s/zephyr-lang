#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_BINARY
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_BINARY 1

#include <memory>

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Binary : Expression {
                    std::unique_ptr<Expression> a, b;
                    enum struct op_t { null, add, sub, mul, div } op = op_t::null;

                    ZEPHYRLANG_DECL_CTOR_3(Binary, a, b, op);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*a); ll.add(*b));

                } ZEPHYRLANG_LISTEN_TYPE(expr::Binary);

            }
        }
    }
}

#endif
