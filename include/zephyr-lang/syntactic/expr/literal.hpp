#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_LITERAL
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_LITERAL 1

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "../../strings.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Literal : Expression {
                    enum struct kind_t { null, int_, uint_, flt_ } kind;
                    union {
                        signed long long int_;
                        unsigned long long uint_;
                        double flt_;
                    } data;

                    Literal() : kind(kind_t::null) {}
                    Literal(decltype(data.int_) int_) : kind(kind_t::int_) { data.int_ = int_; }
                    Literal(decltype(data.uint_) uint_) : kind(kind_t::uint_) { data.uint_ = uint_; }
                    Literal(decltype(data.flt_) flt_) : kind(kind_t::flt_) { data.flt_ = flt_; }

                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl();

                } ZEPHYRLANG_LISTEN_TYPE(expr::Literal);

                struct ZEPHYRLANG_API StringLiteral : Expression {
                    sstr value;

                    ZEPHYRLANG_DECL_CTOR_1(StringLiteral, value);
                    ZEPHYRLANG_DECL_CTOR_D(StringLiteral);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl();

                } ZEPHYRLANG_LISTEN_TYPE(expr::StringLiteral);

            }
        }
    }
}

#endif
