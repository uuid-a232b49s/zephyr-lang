#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_LAMBDA
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_LAMBDA 1

#include <memory>

#include "zephyr-lang/syntactic/listener.hpp"
#include "../function.hpp"
#include "../identifier.hpp"
#include "../stmt/scope.hpp"

#include "expression.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API Lambda : Expression {
                    decltype(Function::args) args;
                    std::unique_ptr<stmt::Scope> body;

                    ZEPHYRLANG_DECL_CTOR_2(Lambda, args, body);
                    ZEPHYRLANG_DECL_CTOR_D(Lambda);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(for (auto & x : args) { ll.add(x); });

                } ZEPHYRLANG_LISTEN_TYPE(expr::Lambda);

            }
        }
    }
}

#endif
