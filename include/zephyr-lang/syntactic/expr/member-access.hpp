#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_MEMBERACCESS
#define HEADER_ZEPHYRLANG_SYNTACTIC_EXPR_MEMBERACCESS 1

#include <memory>
#include <vector>

#include "expression.hpp"
#include "zephyr-lang/syntactic/listener.hpp"
#include "../identifier.hpp"
#include "../../strings.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {
            namespace expr {

                struct ZEPHYRLANG_API MemberAccess : Expression {
                    std::unique_ptr<TypeName> type;
                    std::vector<sstr> fields;

                    ZEPHYRLANG_DECL_CTOR_2(MemberAccess, type, fields);
                    ZEPHYRLANG_DECL_CTOR_D(MemberAccess);
                    ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(*type));

                } ZEPHYRLANG_LISTEN_TYPE(expr::MemberAccess);

            }
        }
    }
}

#endif
