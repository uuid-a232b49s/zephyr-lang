#ifndef HEADER_ZEPHYRLANG_SYNTACTIC_FUNCTION
#define HEADER_ZEPHYRLANG_SYNTACTIC_FUNCTION 1

#include <memory>
#include <vector>

#include "zephyr-lang/syntactic/listener.hpp"
#include "identifier.hpp"
#include "../strings.hpp"
#include "compilation-unit.hpp"

#include "stmt/statement.hpp"

#include "zephyr-lang/internal/api.hpp"
#include "zephyr-lang/internal/decl-ctor.hpp"

namespace zephyr {
    namespace lang {
        namespace syntactic {

            struct ZEPHYRLANG_API FunctionArg : Listenable {
                sstr identifier;
                TypeMeta typeMeta;

                ZEPHYRLANG_DECL_CTOR_2(FunctionArg, identifier, typeMeta);
                ZEPHYRLANG_DECL_CTOR_D(FunctionArg);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl(ll.add(typeMeta));

            } ZEPHYRLANG_LISTEN_TYPE(FunctionArg);

            struct ZEPHYRLANG_API Function : CuDeclaration {
                sstr identifier;
                std::vector<FunctionArg> args;
                TypeMeta ret;
                std::unique_ptr<stmt::Statement> body;

                ZEPHYRLANG_DECL_CTOR_4(Function, identifier, args, ret, body);
                ZEPHYRLANG_DECL_CTOR_D(Function);
                ZEPHYRLANG_SYNTACTIC_LISTENER_impl(
                    for (auto & x : args) ll.add(x);
                    ll.add(ret);
                    ll.add(*body);
                );

            } ZEPHYRLANG_LISTEN_TYPE(Function);

        }
    }
}

#endif
